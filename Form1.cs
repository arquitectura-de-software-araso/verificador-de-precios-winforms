﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProtoLectorPrecios
{   
    public partial class Form1 : Form
    {
        string ruta = @"C:\\img\";
        string codigo = "";
        string[,] productos =
        {
            {"1","Nintendo Switch",  "$5,999.00", "switch.jpg" },
            {"2","XBOX series S",  "$6,099.00", "xboxs.jpg" },
            {"3","XBOX series X",  "$13,499.00", "xboxx.jpg" },
            {"4","Playstation 5 (digital)",  "$12,899.00", "ps5-1.jpg" },
            {"5","Playstation 5 (fisico)",  "$13,999.00", "ps5.jpg" }
        };
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            label1.Location = new Point(this.Width / 2 -label1.Width / 2, 58);
            pictureBox1.Location = new Point(this.Width / 2 - pictureBox1.Width / 2, 
                this.Height / 2 - pictureBox1.Height / 2);
            label2.Location = new Point(this.Width / 2 - label2.Width / 2, 
                this.Height - label2.Height);
        }

        private void Form1_KeyUp(object sender, KeyEventArgs e)
        {
  
        }

        private void Form1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar != 13)
            {
                codigo += e.KeyChar;
            }
            else
            {
                MessageBox.Show("Código" + codigo);
                for (int i = 0; i < 5; i++)
                {
                    if(codigo == productos[i, 0])
                    {
                        // MessageBox.Show("" + productos[i,0] + productos[i, 1]+ productos[i, 2]+ productos[i, 3]+ productos[i, 4]);
                        label3.Text = "Nombre: " + productos[i, 1];
                        label3.Visible = true;

                        label4.Text = "Precio: " + productos[i, 2];
                        label4.Visible = true;

                        pictureBox1.Image = Image.FromFile(ruta + productos[i, 3]);
                    }
                }
                codigo = "";
            }
        }
    }
}
